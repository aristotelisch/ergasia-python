from __future__ import print_function

def brainfuck(code, left, right, data, index):
    """
        brainfuck interpreter
        code: source string
        left: start index
        right: ending index
        data: input data string
        index: start-index of input data string
    """
    output = ""

    if len(code) == 0: return
    if left < 0: left = 0
    if left >= len(code): left = len(code) - 1
    if right < 0: right = 0
    if right >= len(code): right = len(code) - 1

    # tuning machine has infinite array size
    # increase or decrease here accordingly
    arr = [0] * 30000
    ptr = 0
    i = left
    while i <= right:
        s = code[i]
        if s == '>':
            ptr += 1
            # wrap if out of range
            if ptr >= len(arr):
                ptr = 0
        elif s == '<':
            ptr -= 1
            # wrap if out of range
            if ptr < 0:
                ptr = len(arr) - 1
        elif s == '+':
            arr[ptr] += 1
        elif s == '-':
            arr[ptr] -= 1
        elif s == '.':
            output += chr(arr[ptr])
        elif s == ',':
            if index >= 0 and index < len(data):
                arr[ptr] = ord(data[index])
                index += 1
            else:
                arr[ptr] = 0 # out of input
        elif s =='[':
            if arr[ptr] == 0:
                loop = 1
                while loop > 0:
                    i += 1
                    c = code[i]
                    if c == '[':
                        loop += 1
                    elif c == ']':
                        loop -= 1
        elif s == ']':
            loop = 1
            while loop > 0:
                i -= 1
                c = code[i]
                if c == '[':
                    loop -= 1
                elif c == ']':
                    loop += 1
            i -= 1
        i += 1

    return output
