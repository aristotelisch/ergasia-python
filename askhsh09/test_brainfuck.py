import unittest
from brainfuck import brainfuck

class TestBrainfuck(unittest.TestCase):

    def test_hello_world(self):
        source_code = '++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.'
        correct_output = 'Hello World!\n'

        out = brainfuck(source_code, 0, len(source_code) - 1, "", 0)

        self.assertEqual(out, correct_output)

unittest.main()

