from brainfuck import brainfuck

def main():
    code = ''

    print("Type q to quit")
    while code != 'q':

        code = raw_input("> ")

        if code not in ['q']:
            print(brainfuck(code, 0, len(code) - 1, "", 0))

if __name__ == '__main__':
    main()
