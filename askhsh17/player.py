class Player(object):
    """Triliza game player"""
    def __init__(self, name, symbol='X', type='user'):
        self.name = name
        self.symbol = symbol
        self.type = type