import unittest
from player import Player

class TestPlayer(unittest.TestCase):
    """Test Player class functionality"""

    def test_player_name_symbol(self):
        """Test correct initialization of a Player"""
        new_player = Player(name='Aristotelis', symbol='X')

        self.assertEqual('Aristotelis', new_player.name)
        self.assertEqual('X', new_player.symbol)

    def test_player_default_symbol(self):
        """Test default symbol of a Player"""
        new_player = Player(name='Aristotelis')

        self.assertEqual('X', new_player.symbol)
