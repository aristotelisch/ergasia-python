from player import Player
from random import randint

from random import shuffle
import sys

class Game(object):
    """Game of Triliza"""

    def __init__(self, board, player1):
        self.players = []
        self.board = board
        self.player1 = player1
        self.setup_computer_player()
        self.players.append(self.player1)
        self.players.append(self.player2)

    def setup_computer_player(self):
        """Create the computer player and choose an appropriate symbol"""
        if self.player1.symbol == 'X':
            symbol = 'O'
        else:
            symbol = 'X'
        # Creating the computer player
        self.player2 = Player(name='Computer', symbol=symbol, type='computer')

    def valid_position(self, x, y):
        """
        Check if movement is in the board or already played
        Coordinates are zero based
        """
        # TODO: Write tests
        if x < 0 or y < 0 or x > 2 or y > 2:
            return False
        elif self.board[x][y] == '-':
            return True
        elif self.board[x][y] in ['O', 'X']:
            print("The position you entered is not empty")
            return False

    def get_input(self):
        """
        Get the coordinates of the movement from user
        We ask for 1 based coordinates and save them as 0 based for array manipulation
        """
        # TODO: Find solution for compatibility with python 2 and 3 (raw_input vs input)
        move_position = raw_input("Type the coordinates of the box you want to mark (ex. 2 3 for second line third element): ")
        if move_position.lower() == 'q':
            sys.exit()

        try:
            x, y = list(map(lambda z: int(z), move_position.split()))
        except ValueError:
            print ("\nYou entered invalid position coordinates")
            sys.exit()

        return { 'x': x - 1, 'y': y - 1 }

    def play(self, x, y, player):
        if self.valid_position(x, y):
            self.board[x][y] = player.symbol
            return True
        else:
            print('Not a valid movement, try again')
            return False

    def play_computer(self):
        # Find the remaining coordinates to play
        remaining_moves = self.find_remaining_moves()
        shuffle(remaining_moves) # The computeres plays random moves
        self.play(remaining_moves[0][0], remaining_moves[0][1], self.player2)

    def find_remaining_moves(self):
        remaining_moves = [(x, y) for x in [0, 1, 2] for y in [0, 1, 2] if self.board[x][y] == '-']
        return remaining_moves


    def check_winner(self):
        winner = ''
        if ['O', 'O', 'O'] in self.board:
            winner = '0'
        elif ['X', 'X', 'X'] in self.board:
            winner = 'X'
        elif self.board[0][0] == 'O' and self.board[1][0] == 'O' and self.board[2][0] == 'O':
            winner = 'O'
        elif self.board[0][1] == 'O' and self.board[1][1] == 'O' and self.board[2][1] == 'O':
            winner = 'O'
        elif self.board[0][2] == 'O' and self.board[1][2] == 'O' and self.board[2][2] == 'O':
            winner = 'O'
        elif self.board[0][0] == 'X' and self.board[1][0] == 'X' and self.board[2][0] == 'X':
            winner = 'X'
        elif self.board[0][1] == 'X' and self.board[1][1] == 'X' and self.board[2][1] == 'X':
            winner = 'X'
        elif self.board[0][2] == 'X' and self.board[1][2] == 'X' and self.board[2][2] == 'X':
            winner = 'X'
        elif self.board[0][0] == 'O' and self.board[1][1] == 'O' and self.board[2][2] == 'O':
            winner = 'O'
        elif self.board[0][2] == 'O' and self.board[1][1] == 'O' and self.board[2][0] == 'O':
            winner = 'O'
        elif self.board[0][0] == 'X' and self.board[1][1] == 'X' and self.board[2][2] == 'X':
            winner = 'X'
        elif self.board[0][2] == 'X' and self.board[1][1] == 'X' and self.board[2][0] == 'X':
            winner = 'X'

        if winner in ['O','X']:
            print("The winner is " + winner)
        elif winner == '' and len(self.find_remaining_moves()) == 0:
            winner = '-'
            print("We have a tie")

        return winner


    def board_to_str(self):
        game_board_str = ""
        game_board_str += '-' * 11
        game_board_str += '\n'

        for line in self.board:
            game_board_str += "|"
            for item in line:
                game_board_str += ' ' + str(item) + ' '
            game_board_str += "|\n"

        game_board_str += '-' * 11
        game_board_str += '\n'

        return game_board_str

    def print_board(self):
        print(self.board_to_str())
