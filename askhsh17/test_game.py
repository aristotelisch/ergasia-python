import unittest
from game import Game
from player import Player

class TestGame(unittest.TestCase):
    """Test Game functionality"""

    def setUp(self):
        self.player = Player(name='Aristotelis', symbol='X')
        self.game_board = [
            ['-', '-', '-'],
            ['-', '-', '-'],
            ['-', '-', '-']
        ]

    def test_print_initial_game_board(self):
        new_game = Game(self.game_board, self.player)
        game_board_string = '-----------\n| -  -  - |\n| -  -  - |\n| -  -  - |\n-----------\n'
        self.assertEqual(new_game.board_to_str(), game_board_string)