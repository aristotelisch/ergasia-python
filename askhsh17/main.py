from player import Player
from game import Game

game_board = [
       ['-', '-', '-'],
       ['-', '-', '-'],
       ['-', '-', '-']
]

player1_name   = raw_input("Please enter your name: ")
player1_symbol = raw_input("Now choose your symbol(X or O): ")
player1 = Player(player1_name, player1_symbol)
triliza = Game(game_board, player1)
triliza.print_board()

user_played = False

while(True):
    if triliza.check_winner() in ['O', 'X', '-']:
        print("Game over")
        break

    if user_played == False:
        input = triliza.get_input()
        user_played = triliza.play(input['x'], input['y'], triliza.player1)

    elif user_played == True:
        triliza.play_computer()
        user_played = False

    triliza.print_board()
