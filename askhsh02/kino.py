import urllib2
import json
import sys
from calendar import Calendar
from datetime import datetime

class Kino(object):
    """
    Kino game API compare for a given month
    """

    def __init__(self, datetime=datetime.now()):
        self.all_draws_hash = {}
        self.wins_hash = {}
        self.user_input_set = self.get_user_input()
        self.current_day = datetime.day
        self.current_month = datetime.month
        self.current_year = datetime.year
        self.days_of_month_list = Calendar().itermonthdays(self.current_month, self.current_year)

    def build_draws_hash(self):
        for day in range(1, (self.current_day + 1)):
            date = str(day) + '-' + str(self.current_month) + '-' + str(self.current_year)
            self.all_draws_hash[date] = self.get_kino_draws(date)

        # print(self.all_draws_hash)

    def build_wins_hash(self):
        for date, draws in self.all_draws_hash.items():
            self.wins_hash[date] = 0
            # print("Wins: " + str(self.wins_hash[date]))
            # print("Date: " + date)
            # print("Draws: "),
            # print(draws)
            for draw in self.all_draws_hash[date]:
                nums_found_set_length = self.compare_user_with_draw(set(draw))
                if self.user_has_won(nums_found_set_length):
                    self.wins_hash[date] += 1

    def find_max(self):
        print("Dates with maximum wins: ")
        max_win_count = max(self.wins_hash.values())
        for date, wins in self.wins_hash.items():
            if wins == max_win_count:
                print(date + ': ' + str(wins) + ' wins')

    def get_kino_draws(self, date):
        """
        Get kino draws for a particular date by calling the OPAP public rest API
        returns: list of draws
        example: get_kino_date_draws("01-04-2018") # dd-MM-YYYY
        """
        url = "http://applications.opap.gr/DrawsRestServices/kino/drawDate/" + date + ".json"
        response = urllib2.urlopen(url)
        res_json = json.load(response)
        draws = []

        # find the number of results
        draws = res_json['draws']['draw']
        number_of_draws = len(draws)
        # print("Results number: " + str(number_of_draws))
        # print(draws[0]['results'])

        draws = map(lambda x: x['results'], draws)
        # print(draws)

        return draws


    def get_user_input(self):
        """Ask the user to enter 10 integers separated by a space"""
        # User plays 10 numbers, in order to win he has to find more than 4 numbers
        input = raw_input("Give me 10 numbers separated by a space (ex. 1 3 4 5 6 7 8 10 12 10): ")

        # user_kino_numbers_set , we choose a set because the numbers must be unique
        # we also use a lambda function to transform an array of strings to a set of ints
        try:
            user_kino_numbers_set = set(map(lambda x: int(x), input.split()))
        except ValueError:
            print("You should only supply integers")
            print("Please reenter your unique numbers")
            sys.exit()

        if len(user_kino_numbers_set) != 10:
            print("Please reenter your unique numbers")
            input = raw_input("Set not corrent. Please reenter 10 unique numbers separated by a space (ex. 1 3 4 5 6 7 8 10 12 10): ")

        print("User played the following numbers: ")
        print(user_kino_numbers_set)

        return user_kino_numbers_set


    def compare_user_with_draw(self, draw_set):
        """
        Compares the users input with a kino draw set of numbers
        returns: length of numbers matched
        """
        nums_found_set = draw_set.intersection(self.user_input_set)
        nums_found_set_length = len(nums_found_set)

        return nums_found_set_length

    def user_has_won(self, nums_found_set_length):
        if nums_found_set_length > 4:
            # print("You have a winning game by " + str(nums_found_set_length) + " numbers")
            return True
        else:
            # print("You did not win the game, you found " + str(nums_found_set_length) + " numbers")
            return False
