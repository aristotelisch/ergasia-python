from the_calendar import TheCalendar
from datetime import datetime
# import ipdb

def main():
    year = raw_input("Please enter a year (ex. 2000): ")
    month = raw_input("Please enter a month form 1-12(ex. 3): ")

    cal = TheCalendar(1, int(month), int(year))
    cal.print_calendar_month()

    # Test code
    # for i in range(12):
    #     test(i+1, 2000)
    #     print("")

def test(month, year):
    cal = TheCalendar(1, month, year)
    cal.print_calendar_month()

if __name__ == '__main__':
    main()

