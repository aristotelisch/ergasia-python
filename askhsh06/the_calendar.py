"""
Calendar calculations:
Gauss' Method was applicable to the Gregorian calendar. He numbered the weekdays from 0 to 6 starting with Sunday.

R(1+5R(A-1,4)+4R(A-1,100)+6R(A-1,400),7)
where R(y,m) is the remainder after division of y by m.
Reference URL: https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week
"""
from datetime import datetime
from pprint import pprint
# import ipdb # Set trace dynamicaly: ipdb.set_trace()

class TheCalendar(object):

    WEEK_NAMES = {
        "0": 'S',
        "1": 'M',
        "2": 'T',
        "3": 'W',
        "4": 'T',
        "5": 'F',
        "6": 'S'
    }

    MONTH_NAMES = {
        "1": 'January',
        "2": 'February',
        "3": 'March',
        "4": 'April',
        "5": 'May',
        "6": 'Jun',
        "7": 'July',
        "8": 'August',
        "9": 'September',
        "10": 'October',
        "11": 'November',
        "12": 'December'
    }

    MONTH_DAYS_COUNT = {
        "1": 31,
        "2": 28,
        "3": 31,
        "4": 30,
        "5": 31,
        "6": 30,
        "7": 31,
        "8": 31,
        "9": 30,
        "10": 31,
        "11": 30,
        "12": 31
    }

    def __init__(self, day=None, month=None, year=None):
        self.today = datetime.today()
        self.day = day
        self.month = month
        self.year = year

    def get_month(self):
        """Get the month and year from stdin"""

        self.month = raw_input("Please enter the month as an Integer: ")
        self.year  = raw_input("Please enter the year as an Integer: ")

    def print_leap_year(self):
        """Returns a string stating if we have a leap year"""

        if self.is_leap_year():
            leap_message = str(self.year) + ' is a leap year'
        else:
            leap_message = str(self.year) + ' is not a leap year'

        return leap_message

    def is_leap_year(self):
        """Returns True is self.year is a leap year"""

        year = self.year
        if (year % 4 == 0) and (year % 100 != 0) or (year % 400 == 0):
            return True
        else:
            return False

    def weekday_index_calculation(self):
        year_days_list = []

        for month_index in range(1, 13):
            # year_days_list.append(self.MONTH_DAYS_COUNT[str(month_index)])
            year_days_list.append(self.month_days_count(month_index))


        # count the days of the previous months to find
        # the index of the first day of the month withn the year

        days_until_first_of_month = self.first_weekday_calculation()

        for month_index in range(0, self.month-1):
            days_until_first_of_month += year_days_list[month_index]

        # Calculate the weekday index
        weekday_index = days_until_first_of_month % 7

        return weekday_index


    def calendar_month_str(self):
        month = self.month
        year = self.year


        calendar = self.MONTH_NAMES[str(month)] + ' ' + str(year) + '\n'

        weekday_index_num = str(self.weekday_index_calculation())
        month_days = self.month_days_count()


        # Print the Week names header
        for day in range(0, 7):
            calendar += self.WEEK_NAMES[str(day)] + '\t'

        # Print days
        for day in range(month_days+1+int(weekday_index_num)):
            if (day-1) % 7 == 0:
                calendar += '\n'

            day_num = int(day) - int(weekday_index_num)

            # Days that do not belong in the month
            if day_num <= 0:
                calendar += ' ' + '\t'
            else:
                calendar += '{:02d}'.format(day_num) + '\t'

        return calendar

    def print_calendar_month(self):
        print(self.calendar_month_str())

    def first_weekday_calculation(self):
        """
        Determine the 1 January of a specific year day name
        using the Gauss method of calculation for the Gregorian Calendar.
        https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week
        Returns the 0 based index of the week day starting from Sunday
        """

        year = int(self.year)

        first_day_name_index = self.remainder(
            1 + 5 * self.remainder(year-1, 4) +
            4 * self.remainder(year-1, 100) +
            6 * self.remainder(year-1, 400), 7
        )

        return first_day_name_index

    def leap_days_count(self):
        """
        Calculate the number of leap days between two years or
        the current year
        """

        start     = int(self.year)
        end_year  = int(self.year)
        end_month = int(self.month)
        leap_days = 0

        for year in range(start, end_year):
            if is_leap_year():
                leap_days += 1

        if (end_month >= 2 and self.is_leap_year()):
            leap_days += 1

        return leap_days

    def first_weekday_index(self):
        return self.first_weekday_calculation()

    def weekday(self):
        return self.WEEK_NAMES[str(self.first_weekday_index())]

    def month_days_count(self, month=None):
        if month == None:
            month = self.month

        month_days = self.MONTH_DAYS_COUNT[str(month)]

        # month_index 2 is February
        if self.is_leap_year() and month == 2:
            month_days += 1

        return month_days

    def remainder(self, y, m):
        """Syntactic sugar for the modulo operator"""
        return y % m

    # Static Methods
    @staticmethod
    def today():

        return datetime.today()

    # Class Methods
    @classmethod
    def month_to_str(cls, month):

        return cls.MONTH_NAMES[str(month)]

    @classmethod
    def weekday_to_str(cls, weekday):
        print("Weekday " + str(weekday))

        return cls.WEEK_NAMES[str(weekday)]
