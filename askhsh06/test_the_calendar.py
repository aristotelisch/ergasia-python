from unittest import TestCase
from the_calendar import TheCalendar

class TestTheCalendar(TestCase):
    LEAP_YEARS = [1904, 1908, 1912, 1916, 1920, 1924, 1928, 1932, 1936, 1940,
                  1944, 1948, 1952, 1956, 1960, 1964, 1968, 1972, 1976, 1980,
                  1984, 1988, 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020]


    def test_is_leap_year(self):
        for leap_year in self.LEAP_YEARS:
            self.assertTrue(TheCalendar(1,1,leap_year).is_leap_year())

    def test_leap_days_count(self):
        leap_days = 1
        cal = TheCalendar(1, 5, 2000)

        self.assertEqual(cal.leap_days_count(), leap_days)

    def test_no_leap_days_count(self):
        leap_days = 0
        cal = TheCalendar(1, 5, 2018)

        self.assertEqual(cal.leap_days_count(), leap_days)

    def test_print_of_calendar(self):
        """The calendar should print in the terminal"""
        cal = TheCalendar(1,2,2000)
        february_2000_calendar_str = 'February 2000\nS\tM\tT\tW\tT\tF\tS\t \t\n \t \t01\t02\t03\t04\t05\t\n06\t07\t08\t09\t10\t11\t12\t\n13\t14\t15\t16\t17\t18\t19\t\n20\t21\t22\t23\t24\t25\t26\t\n27\t28\t29\t'

        print(february_2000_calendar_str)
        print("The generated calendar is: ")
        print(cal.calendar_month_str())

        self.assertEqual(cal.calendar_month_str(), february_2000_calendar_str)

if __name__ == '__main__':
    unittest.main()
