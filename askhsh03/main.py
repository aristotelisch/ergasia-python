# -*- coding: utf-8 -*-
# Άσκηση 3
# Γράψτε ένα πρόγραμμα σε Python το οποίο παίρνει ένα κείμενο από το χρήστη και κάνει κρυπτογράφηση με το ROT13
# μόνο για τους χαρακτήρες του κειμένου.

import string

def rot13(s):
    """ Encode a string s with the algorithm ROT13"""

    upper = string.ascii_uppercase
    lower = string.ascii_lowercase
    upper_start = ord(upper[0])
    lower_start = ord(lower[0])
    out = ''

    for letter in s:
        if letter in upper:
            out += chr(upper_start + (ord(letter) - upper_start + 13) % 26)
        elif letter in lower:
            out += chr(lower_start + (ord(letter) - lower_start + 13) % 26)
        else:
            out += letter
    return out

def main():
    input = raw_input("Give me the text to encode with rot13: ")
    output_rot13 = rot13(input)
    print("The rot13 encoded text is: ")
    print(output_rot13)

if __name__ == '__main__':
    main()

