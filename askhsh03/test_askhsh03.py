import unittest
import askhsh03 as ex3


class Tests(unittest.TestCase):

    def test_alphabet(self):
        input = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        output = 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm'
        self.assertEqual(ex3.rot13(input), output)

    def test_rot13_with_characters_only(self):
        self.assertEqual(ex3.rot13("bar"), 'one')

    def test_rot13_ignores_numbers(self):
        self.assertEqual(ex3.rot13("bar123"), 'one123')

    def test_rot13_ignores_symbols_and_whitespace(self):
        self.assertEqual(ex3.rot13("Why did the chicken cross the road?"), "Jul qvq gur puvpxra pebff gur ebnq?")

    def test_rot13_call_on_itself_returns_initial_string(self):
        self.assertEqual(ex3.rot13(ex3.rot13("bar")), 'bar')


if __name__ == '__main__':
    unittest.main()
