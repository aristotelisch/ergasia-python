
import os
# require the dotenv library to load sensitive information from an .env file
# This way we avoid saving password into the source code and comminting them to the repository
from dotenv import load_dotenv

# Set the path of the .env file
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

# Twitter client wrapper
from twitter_client import TwitterClient
from text_stats import TextStats

def main():
    """This function runs by default and starts the program"""

    twitter_user = raw_input("\nPlease enter a twitter user's handle(ex. realDonaldTrump): ")
    if twitter_user == '':
        twitter_user = raw_input("\nPlease enter a twitter user's handle(ex. realDonaldTrump): ")
    client = TwitterClient() # TwitterClient object authenticates with credentials from environment variables

    # print(client.verify_authentication())
    statuses_text = client.get_text_statuses_from(twitter_user, count=10)

    statuses = TextStats(statuses_text)
    print("\nThe statuses text file has " + str(statuses.count_words()) + " words")

    most_frequent_word = statuses.most_frequent_word()
    print('The most frequent word is: ' + most_frequent_word + "\n")

if __name__ == '__main__':
    main()
