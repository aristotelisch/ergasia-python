# import ipdb; #used for debugging
import sys

"""Module for text statistics calculation"""

class TextStats(object):
    """Calculate statistics from text"""
    STOP_WORDS = ['TO', 'THE', 'A', 'IS', 'WE', 'AND', 'ARE']

    def __init__(self, text):
        self.text = text

        try:
            self.text_list = map(lambda x: x.upper(), self.text.split())
        except AttributeError as e:
            print(e)
            sys.exit()

        self.text_hash = {}

    def count_words(self):
        return len(self.text_list)

    def most_frequent_word(self):
        """Calculate the most frequent word"""

        # Count only meaningfull words. Remove the stop words from the main text
        words_set = set(self.text_list).difference(self.STOP_WORDS)
        max_word = ''
        max_count = 0

        for word in words_set:
            try:
                temp_count = 0
                for w in self.text_list:
                    if word == w:
                        temp_count += 1

                if temp_count > max_count:
                    max_count = temp_count
                    max_word = word

            except KeyError as e:
                print(e)
                # Trace errors. Requires the library ipdb
                # ipdb.set_trace()
        return max_word







