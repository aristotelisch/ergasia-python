import os
import twitter
import ipdb # ipdb.set_trace()

class TwitterClient(object):

    def __init__(self):
        self.CONSUMER_KEY = os.getenv('CONSUMER_KEY')
        self.CONSUMER_SECRET_KEY = os.getenv('CONSUMER_SECRET_KEY')
        self.ACCESS_TOKEN = os.getenv('ACCESS_TOKEN')
        self.SECRET_ACCESS_TOKEN = os.getenv('SECRET_ACCESS_TOKEN')
        self.api = None
        self.authenticate_twitter()
        self.statuses = None

    def authenticate_twitter(self):
        """Authenticate to Twitter API"""

        self.api = twitter.Api(
            self.CONSUMER_KEY,
            self.CONSUMER_SECRET_KEY,
            self.ACCESS_TOKEN,
            self.SECRET_ACCESS_TOKEN)

        return self.api

    def set_statuses_from(self, twitter_handle='realDonaldTrump', count=10):
        # GetUserTimeline(user_id=None, screen_name=None, since_id=None, max_id=None, count=None, include_rts=True, trim_user=False, exclude_replies=False)
        try:
            self.statuses = self.api.GetUserTimeline(screen_name=twitter_handle, count=count)
            statuses_text_list = [s.text for s in self.statuses]
            return statuses_text_list

        except twitter.error.TwitterError as errors:
            for error in errors.message:
                print('Message ' + error['message'])
                print('Code: '),
                print(error['code'])


    def get_text_statuses_from(self, twitter_handle='realDonaldTrump', count=10):
        """Get the latest user statuses as a joined string"""

        try:
            text = self.set_statuses_from(twitter_handle, count)
            return ''.join(text)
        except TypeError as e:
            print(e)


    def verify_authentication(self):
        return self.api.VerifyCredentials()

